import { Component, OnInit } from '@angular/core';
import { BookDataService } from '../book-data.service';
import { Books } from '../books';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  constructor(private bookService: BookDataService) { }

  ngOnInit() {}

  onSubmit(bookDetails: Books) {
    this.bookService.bookData.push(bookDetails);
}}
