import { Injectable } from '@angular/core';
import { Books } from './books';

@Injectable({
  providedIn: 'root'
})
export class BookDataService {

  public bookData: Books[] = [];
  public bookToEdit: Books;

  constructor() { }

  getBookData() {
    return this.bookData;
  }

  editBook(index: number) {
    this.bookToEdit = this.bookData[index];
  }
}

