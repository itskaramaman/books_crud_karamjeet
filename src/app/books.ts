export class Books {
    constructor(
        public title: string,
        public author: string,
        public description: string,
        public isbn: string,
        public price: number
    ) {}
}
