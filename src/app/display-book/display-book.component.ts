import { Component, OnInit } from '@angular/core';
import { BookDataService } from '../book-data.service';
import { Books } from '../books';
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-display-book',
  templateUrl: './display-book.component.html',
  styleUrls: ['./display-book.component.css']
})
export class DisplayBookComponent implements OnInit {

  public tableHeadings = ['Id', 'Title', 'Author', 'Description', 'ISBN' , 'Price' , 'Deletion', 'Update'];

  public bookData: Books[] = [];

  constructor(private bookService: BookDataService, private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.bookData = this.bookService.getBookData();
  }

  onDelete(index: number) {
    this.bookService.bookData.splice(index, 1);
  }

  onEdit(index: number) {
    this.bookService.editBook(index);
    this.router.navigate(['/updateBook']);
  }
}
