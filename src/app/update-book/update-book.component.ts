import { Component, OnInit } from '@angular/core';
import { BookDataService } from '../book-data.service';
import { Books } from '../books';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {

  public bookDetails: Books;
  public title: string;
  public author: string;
  public description: string;
  public isbn: string;
  public price: number;

  constructor(private updateService: BookDataService, private router: Router) { }

  ngOnInit() {
    this.bookDetails = this.updateService.bookToEdit;
    this.title = this.bookDetails.title;
    this.author = this.bookDetails.author;
    this.description = this.bookDetails.description;
    this.isbn = this.bookDetails.isbn;
    this.price = this.bookDetails.price;
  }

  onSubmit() {
    this.bookDetails.title = this.title;
    this.bookDetails.author = this.author;
    this.bookDetails.description = this.description;
    this.bookDetails.isbn = this.isbn;
    this.bookDetails.price = this.price;
    return this.updateService.getBookData();
  }

  onGoBack() {
    this.router.navigate(['']);
  }
}
